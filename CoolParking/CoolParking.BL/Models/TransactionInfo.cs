﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(decimal sum, string vehicleId)
        {
            TransactionDate = DateTime.Now;
            VehicleId = vehicleId ?? throw new ArgumentException("incorrect id");
            Sum = (sum > 0) ? sum : throw new ArgumentException("incorrect sum");
        }

        public DateTime TransactionDate { get; }
        public string VehicleId { get; }
        public decimal Sum { get; }

        public override string ToString()
        {
            return $"{TransactionDate}: sum - {Sum}, vehicle id - {VehicleId};";
        }
    }
}