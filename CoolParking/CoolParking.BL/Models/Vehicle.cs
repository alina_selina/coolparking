﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using Fare;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly string _idPattern = @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}";

        public string Id { get; }

        public VehicleType VehicleType { get; }

        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = IsCorrectRegistrationPlateNumber(id) ? id : throw new ArgumentException("incorrect id");
            VehicleType = IsCorrectVehicleType(vehicleType) ? vehicleType : throw new ArgumentException("incorrect vehicleType");
            Balance = IsCorrectInitialBalance(balance) ? balance : throw new ArgumentException("incorrect balance");
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var registrationPlateNumberGenerator = new Xeger(_idPattern);
            return registrationPlateNumberGenerator.Generate();
        }

        public static bool IsCorrectRegistrationPlateNumber(string id)
        {
            return id != null && Regex.IsMatch(id, _idPattern);
        }

        public static bool IsCorrectInitialBalance(decimal balance) => balance > 0;

        public static bool IsCorrectVehicleType(VehicleType vehicleType) => Enum.IsDefined(vehicleType);

        public override string ToString()
        {
            return $"Id:{Id},  type: {VehicleType}, balance: {Balance};";
        }
    }
}