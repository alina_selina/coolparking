﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialParkingBalance = 0;
        public const int ParkingCapacity = 10;
        public const int WithdrawIntervalSeconds = 5;
        public const int LogIntervalSeconds = 60;
        public const decimal FineCoefficient = 2.5m;

        public static readonly Dictionary<VehicleType, decimal> TariffList = new()
        {
            {VehicleType.PassengerCar, 2m},
            {VehicleType.Truck, 5m},
            {VehicleType.Bus, 3.5m},
            {VehicleType.Motorcycle, 1m}
        };

        public static readonly string LogPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
    }
}