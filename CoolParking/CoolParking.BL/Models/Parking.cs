﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static readonly Lazy<Parking> _lazy = new(() => new Parking());
        public static Parking Instance => _lazy.Value;

        public decimal Balance { get; set; }
        public int Capacity { get; private set; }
        public List<Vehicle> Vehicles { get; private set; }

        private Parking()
        {
            Balance = Settings.InitialParkingBalance;
            Capacity = Settings.ParkingCapacity;
            Vehicles = new List<Vehicle>();
        }

        public void Reset()
        {
            Balance = Settings.InitialParkingBalance;
            Capacity = Settings.ParkingCapacity;
            Vehicles.Clear();
        }
    }
}