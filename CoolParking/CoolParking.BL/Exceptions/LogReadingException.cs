﻿using System;

namespace CoolParking.BL.Exceptions
{
    public class LogReadingException : Exception
    {
        public LogReadingException()
        {
        }

        public LogReadingException(string message)
            : base(message)
        {
        }

        public LogReadingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
