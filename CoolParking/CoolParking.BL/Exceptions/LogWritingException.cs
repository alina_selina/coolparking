﻿using System;

namespace CoolParking.BL.Exceptions
{
    public class LogWritingException : Exception
    {
        public LogWritingException()
        {
        }

        public LogWritingException(string message)
            : base(message)
        {
        }

        public LogWritingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
