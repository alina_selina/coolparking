﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Exceptions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;
        private ITimerService _logTimer;
        private ITimerService _withdrawTimer;
        private ILogService _logger;
        private List<TransactionInfo> _lastParkingTransactions;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logger)
        {
            _logTimer = logTimer ?? throw new ArgumentException("incorrect value");
            _withdrawTimer = withdrawTimer ?? throw new ArgumentException("incorrect value");
            _logger = logger ?? throw new ArgumentException("incorrect value");

            _parking = Parking.Instance;
            _lastParkingTransactions = new List<TransactionInfo>();

            _withdrawTimer.Elapsed += Withdraw;
            _logTimer.Elapsed += WriteLogs;
            _withdrawTimer.Start();
            _logTimer.Start();
        }

        #region Elapsed Handlers
        private void Withdraw(object sender, ElapsedEventArgs e)
        {
            if (_parking.Vehicles.Count == 0)
                return;

           _parking.Vehicles.ForEach(vehicle => ChargeParkingFee(vehicle));
        }

        private void ChargeParkingFee(Vehicle vehicle)
        {
            if (!Settings.TariffList.TryGetValue(vehicle.VehicleType, out decimal fee))
                return;

            decimal sum = 0m;
            if (vehicle.Balance >= fee)
            {
                sum = fee;
            }
            else if (vehicle.Balance <= 0)
            {
                sum = fee * Settings.FineCoefficient;
            }
            else if (vehicle.Balance > 0 && vehicle.Balance < fee)
            {
                sum = (Math.Abs(vehicle.Balance - fee) * Settings.FineCoefficient) + vehicle.Balance;
            }

            vehicle.Balance -= sum;
            _parking.Balance += sum;
            _lastParkingTransactions.Add(new TransactionInfo(sum, vehicle.Id));
        }

        private void WriteLogs(object sender, ElapsedEventArgs e)
        {
            var log = new StringBuilder();
            _lastParkingTransactions.ForEach(t => log.Append($"{Environment.NewLine}{t}"));
            try
            {
                _logger.Write(log.ToString());
            }
            finally
            {
                _lastParkingTransactions.Clear();
            }
        }
        #endregion

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
                throw new ArgumentException("sum must be positive");

            if (!IsVehicleExists(vehicleId))
                throw new ArgumentException("unexisting vehicle");
            
            GetVehicleById(vehicleId).Balance += sum;
        }

        public decimal GetBalance() => _parking.Balance;

        public int GetCapacity() => _parking.Capacity;

        public int GetFreePlaces() => GetCapacity() - _parking.Vehicles.Count;

        public ReadOnlyCollection<Vehicle> GetVehicles() => _parking.Vehicles.AsReadOnly();

        public TransactionInfo[] GetLastParkingTransactions() => _lastParkingTransactions.ToArray();

        public decimal GetLastParkingTransactionsSum() => _lastParkingTransactions.Sum(t => t.Sum);

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0)
                throw new InvalidOperationException("parking is full");

            if (!IsVehicleIdUnique(vehicle))
                throw new ArgumentException("existing vehicle Id");
            
            _parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!IsVehicleExists(vehicleId))
                throw new ArgumentException("unexisting vehicle");

            var vehicle = GetVehicleById(vehicleId);
            if (vehicle.Balance < 0)
                throw new InvalidOperationException("you cannot remove a car with negative balance");
            
            _parking.Vehicles.Remove(vehicle);
        }

        #region Helpers
        private bool IsVehicleIdUnique(Vehicle vehicle)
        {
            return !_parking.Vehicles.Exists(v => v.Id == vehicle?.Id);
        }

        private bool IsVehicleExists(string vehicleId)
        {
            return _parking.Vehicles.Exists(v => v.Id == vehicleId);
        }

        private Vehicle GetVehicleById(string vehicleId)
        {
            return _parking.Vehicles.Find(v => v.Id == vehicleId);
        }
        #endregion

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _lastParkingTransactions.Clear();
            _parking.Reset();
        }

        public string ReadFromLog()
        {
            return _logger.Read();
        }
    }
}