﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System;
using System.IO;
using CoolParking.BL.Exceptions;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath ?? throw new ArgumentNullException("logPath cannot be null");
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("logFile doesn't exist");
            try
            {
                using var streamReader = new StreamReader(LogPath);
                var logInfo = streamReader.ReadToEnd();
                return logInfo;
            }
            catch (Exception)
            {
                throw new LogReadingException("error reading log file");
            }
        }

        public void Write(string logInfo)
        {
            if (string.IsNullOrEmpty(logInfo))
                return;

            try
            {
                using var streamWriter = new StreamWriter(LogPath, true);
                streamWriter.WriteLine(logInfo);
            }
            catch (Exception)
            {
                throw new LogWritingException("error writing to log file");
            }
        }
    }
}
