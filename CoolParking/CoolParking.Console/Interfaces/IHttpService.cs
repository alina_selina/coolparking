﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.Interfaces
{
    public interface IHttpService : IDisposable
    {
        Task<HttpResponseMessage> Get(string path, int? timeout);
        Task<HttpResponseMessage> Post(string path, HttpContent data, int? timeout);
        Task<HttpResponseMessage> Put(string path, HttpContent data, int? timeout);
        Task<HttpResponseMessage> Delete(string path, int? timeout);
    }
}
