﻿namespace CoolParking.ConsoleApp.Interfaces
{
    public interface ICommand
    {
        void Execute();
    }
}
