﻿using static System.Console;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.ConsoleInterface.Decorators;
using CoolParking.ConsoleApp.Interfaces;
using Newtonsoft.Json;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class ShowTransactionsHistory : HttpCommand
    {
        public ShowTransactionsHistory(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowTransactionsHistory(IHttpService httpService, string path, int timeout): base(httpService, path, timeout)
        { 
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            var response = Wait(() => Client.Get(Path, Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response, false);
                return;
            }

            var allTransactions = JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
            ExecuteActionAndWaitForAnyKeyPress(() => WriteLine($"\nTransactions history:\n{allTransactions}"));
        }
    }
}
