﻿using static System.Console;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.ConsoleInterface.Decorators;
using CoolParking.ConsoleApp.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class ShowLastParkingTransactions : HttpCommand
    {
        public ShowLastParkingTransactions(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowLastParkingTransactions(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            var response = Wait(() => Client.Get(Path, Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response, false);
                return;
            }

            var lastTransactions = JsonConvert.DeserializeObject<List<TransactionDto>>(response.Content.ReadAsStringAsync().Result);
            
            WriteLine("\nLast transactions:\n");
            if (lastTransactions.Count > 0)
                lastTransactions.ForEach(t => WriteLine(t.ToString()));
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }
    }
}
