﻿using CoolParking.ConsoleApp.Interfaces;
using System;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class Command : ICommand
    {
        private Action Action { get; }

        public Command(Action action)
        {
            Action = action;
        }

        public void Execute()
        {
            Action.Invoke();
        }
    }
}
