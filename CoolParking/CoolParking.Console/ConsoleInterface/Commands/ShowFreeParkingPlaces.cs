﻿using CoolParking.ConsoleApp.Interfaces;
using static System.Console;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.ConsoleInterface.Decorators;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class ShowFreeParkingPlaces : HttpCommand
    {
        public ShowFreeParkingPlaces(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowFreeParkingPlaces(IHttpService httpService, string path, int timeout): base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            var response = Wait(() => Client.Get(Path, Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response, false);
                return;
            }

            ExecuteActionAndWaitForAnyKeyPress(() => WriteLine($"\nFree places: {response.Content.ReadAsStringAsync().Result}"));
        }
    }
}
