﻿using static System.Console;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using Newtonsoft.Json;
using System.Collections.Generic;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.ConsoleInterface.Decorators;
using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class ShowVehicleList : HttpCommand
    {
        public ShowVehicleList(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowVehicleList(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            var response = Wait(() => Client.Get(Path, Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response, false);
                return;
            }

            var vehicles = JsonConvert.DeserializeObject<List<VehicleDto>>(response.Content.ReadAsStringAsync().Result);

            WriteLine("\nVehicles:\n");
            if (vehicles.Count > 0)
                vehicles.ForEach(v => WriteLine(v.ToString()));
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }
    }
}
