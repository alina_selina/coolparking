﻿using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.ConsoleInterface.Decorators;
using CoolParking.ConsoleApp.Interfaces;
using static CoolParking.ConsoleApp.ConsoleInterface.Validators.ConsoleValidators;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class GetVehicle : HttpCommand
    {
        public GetVehicle(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public GetVehicle(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        { 
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            if (!TryGetVehicleIdInput(out string id))
                return;

            var response = Wait(() => Client.Get($"{Path}/{id}", Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response, false);
                return;
            }

            ShowHttpResponse<VehicleDto>(response, true, "\nVehicle:");
        }
    }
}
