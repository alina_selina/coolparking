﻿using static CoolParking.ConsoleApp.ConsoleInterface.Validators.ConsoleValidators;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.ConsoleInterface.Decorators;
using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class TopUpVehicle : HttpCommand
    {
        public TopUpVehicle(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public TopUpVehicle(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            if (!TryGetTopUpVehicleParams(out string id, out decimal sum))
                return;

            var json = JsonConvert.SerializeObject(new TopUpVehicleDto { Id = id, Sum = sum });
            using var data = new StringContent(json, Encoding.UTF8, "application/json");
            using var response = Wait(() => Client.Put(Path, data, Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response, false);
                return;
            }

            ShowHttpResponse<VehicleDto>(response, true, "\nYour car balance has been replenished.");
        }

        private bool TryGetTopUpVehicleParams(out string id, out decimal sum)
        {
            if (!TryGetVehicleIdInput(out id))
            {
                sum = 0;
                return false;
            }

            if (!TryGetSumInput(out sum))
                return false;

            return true;
        }
    }
}
