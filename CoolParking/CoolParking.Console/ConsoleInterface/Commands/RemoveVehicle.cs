﻿using static CoolParking.ConsoleApp.ConsoleInterface.Validators.ConsoleValidators;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleFormatter;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.ConsoleInterface.Decorators;
using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class RemoveVehicle : HttpCommand
    {
        public RemoveVehicle(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public RemoveVehicle(IHttpService httpService, string path, int timeout): base(httpService, path, timeout)
        { 
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            if (!TryGetVehicleIdInput(out string id))
                return;

            using var response = Wait(() => Client.Delete($"{Path}/{id}", Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response, false);
                return;
            }

            ExecuteActionAndWaitForAnyKeyPress(() => WriteSuccessMessage("\nYour vehicle removed from parking", true));
        }
    }
}
