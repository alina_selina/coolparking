﻿using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    public abstract class HttpCommand : ICommand
    {
        protected IHttpService Client { get; }
        protected string Path { get; }
        protected int? Timeout { get; }

        public HttpCommand(IHttpService httpService, string path)
        {
            Client = httpService;
            Path = path;
        }

        public HttpCommand(IHttpService httpService, string path, int timeout)
        {
            Client = httpService;
            Path = path;
            Timeout = timeout;
        }

        public abstract void Execute();
    }
}
