﻿using static CoolParking.ConsoleApp.ConsoleInterface.Validators.ConsoleValidators;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.ConsoleInterface.Decorators;
using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp.ConsoleInterface.Commands
{
    class AddVehicle : HttpCommand
    {
        public AddVehicle(IHttpService httpService, string path) : base(httpService, path)
        { 
        }

        public AddVehicle(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            if (!TryGetVehicleParams(out string id, out int type, out decimal balance))
                return;

            var json = JsonConvert.SerializeObject(new VehicleDto(id, type, balance));
            using var data = new StringContent(json, Encoding.UTF8, "application/json");

            using var response = Wait(() => Client.Post(Path, data, Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response, false);
                return;
            }

            ShowHttpResponse<VehicleDto>(response, true, "\nYour vehicle added on parking.");
        }

        private bool TryGetVehicleParams(out string id, out int type, out decimal balance)
        {
            if (!TryGetVehicleIdInput(out id))
            {
                type = 0;
                balance = 0;
                return false;
            }

            if (!TryGetVehicleTypeInput(out type))
            {
                balance = 0;
                return false;
            }

            if (!TryGetBalanceInput(out balance))
                return false;

            return true;
        }
    }
}
