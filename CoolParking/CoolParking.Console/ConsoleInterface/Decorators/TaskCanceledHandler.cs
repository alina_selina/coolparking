﻿using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleFormatter;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using PostSharp.Aspects;
using PostSharp.Serialization;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp.ConsoleInterface.Decorators
{
    [PSerializable]
    public class TaskCanceledHandler : MethodInterceptionAspect
    {
        public override void OnInvoke(MethodInterceptionArgs args)
        {
            try
            {
                args.Proceed();
            }
            catch (TaskCanceledException)
            {
                ExecuteActionAndWaitForAnyKeyPress(() => WriteErrorMessage("Timeout", true));
            }
        }
    }
}
