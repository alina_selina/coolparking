﻿using PostSharp.Aspects;
using PostSharp.Serialization;
using System.Net.Http;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleFormatter;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;

namespace CoolParking.ConsoleApp.ConsoleInterface.Decorators
{
    [PSerializable]
    public class HttpErrorHandler : MethodInterceptionAspect
    {
        public override void OnInvoke(MethodInterceptionArgs args)
        {
            try
            {
                args.Proceed();
            }
            catch (HttpRequestException ex)
            {
                ExecuteActionAndWaitForAnyKeyPress(() => WriteErrorMessage($"Error: {ex.Message}", true));
            }
        }
    }
}
