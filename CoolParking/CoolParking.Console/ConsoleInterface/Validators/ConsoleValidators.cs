﻿using static System.Console;
using System.Collections.Generic;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;
using System.Text.RegularExpressions;

namespace CoolParking.ConsoleApp.ConsoleInterface.Validators
{
    public static class ConsoleValidators
    {
        public static bool TryGetVehicleIdInput(out string id)
        {
            WriteLine($"\nEnter your vehicle registration number (format: XX-1111-YY)");
            while (true)
            {
                id = ReadLine();
                if (Regex.IsMatch(id, @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
                    return true;

                else if (!IsTryingGetValueAgain("Incorrect vehicle registration number."))
                {
                    id = null;
                    return false;
                }
            }
        }

        public static bool TryGetVehicleTypeInput(out int type)
        {
            WriteLine("\nChoose the vehicle type:");
            var vehicleTypes = new Dictionary<int, string>()
            {
                { 0, "PassengerCar" },
                { 1, "Truck" },
                { 2, "Bus" },
                { 3, "Motorcycle" }
            };

            foreach (KeyValuePair<int, string> vehicleType in vehicleTypes)
                WriteLine($"{vehicleType.Key} - {vehicleType.Value}");

            while (true)
            {
                if (int.TryParse(ReadLine(), out type) && vehicleTypes.ContainsKey(type))
                    return true;

                else if (!IsTryingGetValueAgain("Incorrect value, unexist type."))
                {
                    type = 0;
                    return false;
                }
            }
        }

        public static bool TryGetBalanceInput(out decimal balance)
        {
            WriteLine("\nEnter the vehicle balance:");

            while (true)
            {
                decimal.TryParse(ReadLine(), out balance);
                if (balance > 0)
                    return true;

                else if (!IsTryingGetValueAgain("Incorrect value, balance must be positive."))
                {
                    balance = 0;
                    return false;
                }
            }
        }

        public static bool TryGetSumInput(out decimal sum)
        {
            WriteLine("\nEnter sum:");

            while (true)
            {
                decimal.TryParse(ReadLine(), out sum);
                if (sum > 0)
                    return true;

                else if (!IsTryingGetValueAgain("Incorrect value, value must be positive."))
                {
                    sum = 0;
                    return false;
                }
            }
        }
    }
}
