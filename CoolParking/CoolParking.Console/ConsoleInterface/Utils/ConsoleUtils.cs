﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using static System.Console;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleFormatter;

namespace CoolParking.ConsoleApp.ConsoleInterface.Utils
{
    public static class ConsoleUtils
    {
        public static bool IsTryingGetValueAgain(string message)
        {
            while (true)
            {
                ClearLine();
                WriteWarningMessage($"{message} Try again? [y/n]: ");

                var input = ReadLine();
                if (input == "y")
                {
                    ClearLine();
                    return true;
                }
                else if (input == "n")
                {
                    ClearLine();
                    return false;
                }
            }
        }

        public static void ExecuteActionAndWaitForAnyKeyPress(Action action, string message = "")
        {
            action();

            if (string.IsNullOrEmpty(message))
                WaitForAnyKeyPress();
            else
                WaitForAnyKeyPress(message);
        }

        public static void WaitForAnyKeyPress(string message = "\nPress any key to continue...")
        {
            WriteLine(message);
            ReadKey();
        }

        public static T Wait<T>(Func<T> func)
        {
            WriteWarningMessage("Loading...", true);
            try
            {
                return func.Invoke();
            }
            finally
            {
                ClearLine();
            }
        }

        public static void ShowHttpResponse<T>(HttpResponseMessage response, bool isSuccess, string message = "")
        {
            var result = response.Content.ReadAsStringAsync().Result;
            var obj = JsonConvert.DeserializeObject<T>(result);

            if (isSuccess)
                ExecuteActionAndWaitForAnyKeyPress(() => WriteSuccessMessage($"\n{message}\n{obj}", true));
            else
                ExecuteActionAndWaitForAnyKeyPress(() => WriteErrorMessage($"\n{message}\n{obj}", true));
        }
    }
}
