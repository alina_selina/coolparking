﻿using System;
using System.Net;
using System.Net.Http;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleFormatter;
using static CoolParking.ConsoleApp.ConsoleInterface.Utils.ConsoleUtils;

namespace CoolParking.ConsoleApp.ConsoleInterface.Utils
{
    public static class HttpErrorHandler
    {
        public static void Handle(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.InternalServerError)
            {
                ExecuteActionAndWaitForAnyKeyPress(() => WriteErrorMessage($"InternalServerError: {ex.Message}", true));
            }
            catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
            {
                ExecuteActionAndWaitForAnyKeyPress(() => WriteErrorMessage($"NotFound: {ex.Message}", true));
            }
            catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.BadRequest)
            {
                ExecuteActionAndWaitForAnyKeyPress(() => WriteErrorMessage($"BadRequest: {ex.Message}", true));
            }
            catch (HttpRequestException ex)
            {
                ExecuteActionAndWaitForAnyKeyPress(() => WriteErrorMessage($"Error: {ex.Message}", true));
            }
        }
    }
}
