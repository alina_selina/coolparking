﻿using CoolParking.ConsoleApp.Models;
using System;
using static System.Console;

namespace CoolParking.ConsoleApp.ConsoleInterface.Utils
{
    public static class ConsoleFormatter
    {
        public static void WriteErrorMessage(string message, bool newLine=false)
        {
            WriteMessage(message, Settings.ErrorMessageColor, newLine);
        }

        public static void WriteSuccessMessage(string message, bool newLine = false)
        {
            WriteMessage(message, Settings.SuccessMessageColor, newLine);
        }

        public static void WriteWarningMessage(string message, bool newLine = false)
        {
            WriteMessage(message, Settings.WarningMessageColor, newLine);
        }

        public static void WriteMessage(string message, ConsoleColor color, bool newLine = false)
        {
            ForegroundColor = color;
            if (newLine)
                WriteLine(message);
            else
                Write(message);
            ResetColor();
        }

        public static void ClearLine()
        {
            SetCursorPosition(0, CursorTop - 1);
            Write(new string(' ', WindowWidth));
            SetCursorPosition(0, CursorTop - 1);
        }
    }
}
