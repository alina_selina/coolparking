﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace CoolParking.ConsoleApp.Models
{
    public class TransactionDto
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [JsonProperty("sum")]
        [JsonNumberHandling(JsonNumberHandling.Strict)]
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }

        public override string ToString()
        {
            return $"{TransactionDate}: sum - {Sum}, vehicle id - {VehicleId};";
        }
    }
}
