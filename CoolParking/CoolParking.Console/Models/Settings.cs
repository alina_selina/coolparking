﻿using System;

namespace CoolParking.ConsoleApp.Models
{
    public static class Settings
    {
        public const ConsoleColor ErrorMessageColor = ConsoleColor.Red;
        public const ConsoleColor SuccessMessageColor = ConsoleColor.Green;
        public const ConsoleColor WarningMessageColor = ConsoleColor.Yellow;

        public const string BaseAddress = "https://localhost:44344";
        public const int Timeout = 60 * 1000;
    }
}
