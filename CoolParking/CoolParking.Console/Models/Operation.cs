﻿using CoolParking.ConsoleApp.Interfaces;

namespace CoolParking.ConsoleApp.Models
{
    public struct Operation
    {
        public string Name { get; set; }
        public ICommand Command { get; set; }

        public Operation(string name, ICommand method)
        {
            Name = name;
            Command = method;
        }
    }
}
