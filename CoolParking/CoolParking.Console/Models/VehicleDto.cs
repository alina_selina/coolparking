﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace CoolParking.ConsoleApp.Models
{
    public class VehicleDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }

        [JsonProperty("balance")]
        [JsonNumberHandling(JsonNumberHandling.Strict)]
        public decimal Balance { get; set; }

        public VehicleDto(string id, int type, decimal balance)
        {
            Id = id;
            Balance = balance;
            VehicleType = type;
        }

        public override string ToString()
        {
            return $"Id:{Id}, type: {VehicleType}, balance: {Balance};";
        }
    }
}
