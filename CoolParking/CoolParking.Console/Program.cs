﻿using CoolParking.ConsoleApp.ConsoleInterface;
using CoolParking.ConsoleApp.ConsoleInterface.Commands;
using CoolParking.ConsoleApp.Services;
using System.Collections.Generic;
using System;
using CoolParking.ConsoleApp.Models;
using static CoolParking.ConsoleApp.Models.Settings;

namespace CoolParking.ConsoleApp
{
    class Program
    {
        private static Dictionary<int, Operation> _mainMenu;
        private static Dictionary<int, Operation> _parkingMenu;
        private static Dictionary<int, Operation> _transactionsMenu;
        private static Dictionary<int, Operation> _vehicleOperationsMenu;
        private static string _header = "COOL PARKING";

        private static HttpService httpService;

        static Program()
        {
            httpService = new HttpService() { BaseAddress = new Uri(BaseAddress), Timeout = TimeSpan.FromMilliseconds(Timeout) };
            InitializeMenus();
        }

        static void Main()
        {
            StartMenu(_mainMenu, _header);
        }

        public static void StartMenu(Dictionary<int, Operation> menu, string header)
        {
            while (true)
            {
                Console.Clear();
                Menu.ShowHeader(header);
                Menu.Show(menu);
                Menu.SelectOperation(menu);
            }
        }

        static void InitializeMenus()
        {
            _vehicleOperationsMenu = new Dictionary<int, Operation>
            {
                {1, new Operation("Add vehicle", new AddVehicle(httpService, "/api/vehicles"))},
                {2, new Operation("Remove vehicle", new RemoveVehicle(httpService, "/api/vehicles"))},
                {3, new Operation("Get vehicle", new GetVehicle(httpService, "/api/vehicles"))},
                {4, new Operation("Vehicle list", new ShowVehicleList(httpService, "/api/vehicles"))},
                {5, new Operation("Go back", new Command(()=>StartMenu(_parkingMenu, _header)))}
            };

            _mainMenu = new Dictionary<int, Operation>
            {
                {1, new Operation("Parking Menu", new Command(()=>StartMenu(_parkingMenu, _header)))},
                {2, new Operation("Transactions Menu", new Command(()=>StartMenu(_transactionsMenu, _header)))},
                {3, new Operation("Quit", new Command(()=>Environment.Exit(0))) }
            };

            _parkingMenu = new Dictionary<int, Operation>
            {
                {1, new Operation("Parking balance", new ShowParkingBalance(httpService, "/api/parking/balance"))},
                {2, new Operation("Parking capacity", new ShowParkingCapacity(httpService, "/api/parking/capacity"))},
                {3, new Operation("Free parking places",new ShowFreeParkingPlaces(httpService, "/api/parking/freePlaces"))},
                {4, new Operation("Vehicle operations menu", new Command(()=>StartMenu(_vehicleOperationsMenu, _header)))},
                {5, new Operation("Go back", new Command(()=>StartMenu(_mainMenu, _header)))}
            };

            _transactionsMenu = new Dictionary<int, Operation>
            {
                {1, new Operation("Top up vehicle balance", new TopUpVehicle(httpService, "/api/transactions/topUpVehicle"))},
                {2, new Operation("Last transactions", new ShowLastParkingTransactions(httpService, "/api/transactions/last"))},
                {3, new Operation("Transactions history", new ShowTransactionsHistory(httpService, "/api/transactions/all"))},
                {4, new Operation("Go back", new Command(()=>StartMenu(_mainMenu, _header)))}
            };
        }
    }
}
