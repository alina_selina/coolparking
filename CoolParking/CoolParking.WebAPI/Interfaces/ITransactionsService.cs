﻿using CoolParking.BL.Models;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ITransactionsService
    {
        IEnumerable<TransactionInfo> GetLastTransactions();
        string GetAllTransactions();
        Vehicle TopUpVehicle(string vehicleId, decimal sum);
    }
}
