﻿using CoolParking.BL.Models;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IVehiclesService
    {
        IEnumerable<Vehicle> GetVehicles();
        Vehicle GetVehicle(string id);
        Vehicle AddVehicle(Vehicle vehicle);
        void RemoveVehicle(string id);
    }
}
