﻿using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Validators
{
    public static class VehicleIdValidator
    {
        public static bool IsValid(string id) => Vehicle.IsCorrectRegistrationPlateNumber(id);
    }
}
