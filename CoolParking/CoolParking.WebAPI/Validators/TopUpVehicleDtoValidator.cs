﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using FluentValidation;

namespace CoolParking.WebAPI.Validators
{
    public class TopUpVehicleDtoValidator : AbstractValidator<TopUpVehicleDto>
    {
        public TopUpVehicleDtoValidator()
        {
            RuleFor(x => x.Id).Must(Vehicle.IsCorrectRegistrationPlateNumber).WithMessage("Incorrect id");
            RuleFor(x => x.Sum).Must(x => x > 0).WithMessage("Incorrect sum");
        }
    }
}
