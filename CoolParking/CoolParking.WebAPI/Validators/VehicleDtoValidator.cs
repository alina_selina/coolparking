﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using FluentValidation;

namespace CoolParking.WebAPI.Validators
{
    public class VehicleDtoValidator : AbstractValidator<VehicleDto>
    {
        public VehicleDtoValidator()
        {
            RuleFor(x => x.Id).Must(Vehicle.IsCorrectRegistrationPlateNumber).WithMessage("Incorrect id");
            RuleFor(x => x.VehicleType).Must(Vehicle.IsCorrectVehicleType).WithMessage("Incorrect type");
            RuleFor(x => x.Balance).Must(Vehicle.IsCorrectInitialBalance).WithMessage("Incorrect balance");
        }
    }
}
