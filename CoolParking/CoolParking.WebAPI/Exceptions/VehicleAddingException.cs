﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Exceptions
{
    public class VehicleAddingException : Exception
    {
        public VehicleAddingException()
        {
        }

        public VehicleAddingException(string message)
            : base(message)
        {
        }

        public VehicleAddingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
