﻿using System.Collections.Generic;
using CoolParking.BL.Models;
using System.Linq;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Exceptions;

namespace CoolParking.WebAPI.Services
{
    public class TransactionsService : ITransactionsService
    {
        private readonly BL.Interfaces.IParkingService _parking;

        public TransactionsService(BL.Interfaces.IParkingService parking)
        {
            _parking = parking;
        }

        public IEnumerable<TransactionInfo> GetLastTransactions()
        {
            return _parking.GetLastParkingTransactions();
        }

        public string GetAllTransactions() => _parking.ReadFromLog();

        public Vehicle TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.GetVehicles().FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
                throw new ObjectNotFoundException("Vehicle not found");

            _parking.TopUpVehicle(vehicleId, sum);
            return vehicle;
        }
    }
}

