﻿using System.Collections.Generic;
using CoolParking.BL.Models;
using System.Linq;
using System;
using CoolParking.WebAPI.Exceptions;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class VehiclesService : IVehiclesService
    {
        private readonly BL.Interfaces.IParkingService _parking;

        public VehiclesService(BL.Interfaces.IParkingService parking)
        {
            _parking = parking;
        }

        public IEnumerable<Vehicle> GetVehicles() => _parking.GetVehicles();

        public Vehicle GetVehicle(string id)
        {
            return _parking.GetVehicles().FirstOrDefault(v => v.Id == id);
        }

        public Vehicle AddVehicle(Vehicle vehicle)
        {
            try
            {
                _parking.AddVehicle(vehicle);
                return _parking.GetVehicles().FirstOrDefault(v => v.Id == vehicle.Id);
            }
            catch (ArgumentException e)
            {
                throw new VehicleAddingException(e.Message);
            }
            catch (InvalidOperationException e)
            {
                throw new VehicleAddingException(e.Message);
            }
        }

        public void RemoveVehicle(string id)
        {
            _parking.RemoveVehicle(id);
        }
    }
}
