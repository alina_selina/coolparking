﻿using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class ParkingService : IParkingService
    {
        private readonly BL.Interfaces.IParkingService _parking;

        public ParkingService(BL.Interfaces.IParkingService parking)
        {
            _parking = parking;
        }

        public decimal GetBalance() => _parking.GetBalance();

        public int GetCapacity() => _parking.GetCapacity();

        public int GetFreePlaces() => _parking.GetFreePlaces();
    }
}
