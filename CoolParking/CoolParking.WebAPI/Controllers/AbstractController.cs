﻿using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public abstract class AbstractController<TService> : ControllerBase
    {
        protected readonly TService _service;

        protected AbstractController(TService service)
        {
            _service = service;
        }
    }
}
