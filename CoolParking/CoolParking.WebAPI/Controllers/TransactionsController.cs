﻿using AutoMapper;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Exceptions;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;

namespace CoolParking.WebAPI.Controllers
{
    public class TransactionsController : AbstractController<ITransactionsService>
    {
        private readonly IMapper _mapper;

        public TransactionsController(ITransactionsService transactionsService, IMapper mapper):base(transactionsService)
        {
            _mapper = mapper;
        }

        // GET api/transactions/last
        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionDto>> GetLastTransactions()
        {
            var transactions = _service.GetLastTransactions();
            return Ok(_mapper.Map<IEnumerable<TransactionInfo>, IEnumerable<TransactionDto>>(transactions));
        }

        // GET api/transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(_service.GetAllTransactions());
            }
            catch (InvalidOperationException e)
            {
                return NotFound(new HttpErrorResponse(HttpStatusCode.NotFound, e.Message));
            }
        }

        //PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<string> TopUpVehicle([FromBody] TopUpVehicleDto topUpVehicleDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(new HttpErrorResponse(HttpStatusCode.BadRequest, "Incorrect data"));

            try
            {
                var vehicle = _service.TopUpVehicle(topUpVehicleDto.Id, topUpVehicleDto.Sum);
                return Ok(_mapper.Map<Vehicle, VehicleDto>(vehicle));
            }
            catch (ObjectNotFoundException e)
            {
                return NotFound(new HttpErrorResponse(HttpStatusCode.NotFound, e.Message));
            }
            catch (ArgumentException e)
            {
                return BadRequest(new HttpErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }
    }
}
