﻿using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    public class ParkingController : AbstractController<IParkingService>
    {
        public ParkingController(IParkingService parkingService):base(parkingService)
        {
        }

        // GET api/parking/balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance() => Ok(_service.GetBalance());

        // GET api/parking/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity() => Ok(_service.GetCapacity());

        // GET api/parking/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces() => Ok(_service.GetFreePlaces());
    }
}
