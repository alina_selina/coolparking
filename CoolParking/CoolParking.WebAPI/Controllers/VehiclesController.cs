﻿using AutoMapper;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Exceptions;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Validators;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;

namespace CoolParking.WebAPI.Controllers
{
    public class VehiclesController : AbstractController<IVehiclesService>
    {
        private readonly IMapper _mapper;

        public VehiclesController(IVehiclesService vehiclesService, IMapper mapper):base(vehiclesService)
        {
            _mapper = mapper;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> Get()
        {
            var vehicles = _service.GetVehicles();
            return Ok(_mapper.Map<IEnumerable<Vehicle>, IEnumerable<VehicleDto>>(vehicles));
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            if (!VehicleIdValidator.IsValid(id))
                return BadRequest(new HttpErrorResponse(HttpStatusCode.BadRequest, "Incorrect id"));

            var vehicle = _service.GetVehicle(id);
            if (vehicle == null)
                return NotFound(new HttpErrorResponse(HttpStatusCode.NotFound, "Vehicle not found"));

            return Ok(_mapper.Map<Vehicle, VehicleDto>(vehicle));
        }

        //POST api/vehicles
        [HttpPost]
        public ActionResult<VehicleDto> Post([FromBody] VehicleDto vehicleDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(new HttpErrorResponse(HttpStatusCode.BadRequest, "Incorrect data"));

            try
            {
                var vehicle = _service.AddVehicle(_mapper.Map<VehicleDto, Vehicle>(vehicleDto));

                var uri = new Uri($"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.Path}/{vehicle.Id}");

                return Created(uri, _mapper.Map<Vehicle, VehicleDto>(vehicle));
            }
            catch (VehicleAddingException e)
            {
                return BadRequest(new HttpErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        //DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public ActionResult<Vehicle> Delete(string id)
        {
            if (!VehicleIdValidator.IsValid(id))
                return BadRequest(new HttpErrorResponse(HttpStatusCode.BadRequest, "Incorrect id"));

            try
            {
                _service.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(new HttpErrorResponse(HttpStatusCode.NotFound, e.Message));
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(new HttpErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }
    }
}
