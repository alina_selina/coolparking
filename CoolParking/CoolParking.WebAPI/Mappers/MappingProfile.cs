﻿using AutoMapper;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Mappers
{
    namespace MyMusic.Api.Mapping
    {
        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<Vehicle, VehicleDto>();
                CreateMap<TransactionInfo, TransactionDto>();

                CreateMap<TransactionDto, TransactionInfo>();
                CreateMap<VehicleDto, Vehicle>();
            }
        }
    }
}
