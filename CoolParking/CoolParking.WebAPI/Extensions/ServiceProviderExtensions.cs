﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CoolParking.WebAPI.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static void AddParking(this IServiceCollection services)
        {
            var logService = new LogService(Settings.LogPath);
            var withdrawTimer = new TimerService(Settings.WithdrawIntervalSeconds * 1000);
            var logTimer = new TimerService(Settings.LogIntervalSeconds * 1000);
            var parkingService = new ParkingService(withdrawTimer, logTimer, logService);

            services.AddSingleton<IParkingService>(x => parkingService);
        }
    }
}
