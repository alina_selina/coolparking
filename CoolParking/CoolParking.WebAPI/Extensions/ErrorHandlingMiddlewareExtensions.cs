﻿using CoolParking.WebAPI.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace CoolParking.WebAPI.Extensions
{
    public static class ErrorHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseErrorHandler(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ErrorHandlingMiddleware>();
        }
    }
}
