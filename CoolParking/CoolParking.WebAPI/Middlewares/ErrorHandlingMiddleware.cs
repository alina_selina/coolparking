﻿using CoolParking.WebAPI.Exceptions;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch(Exception e)
            {
                await HandleErrorAsync(context, e);
            }
        }

        private async Task HandleErrorAsync(HttpContext context, Exception exception)
        {
            var errorResponse = new HttpErrorResponse();

            if (exception is HttpException httpException)
            {
                errorResponse.StatusCode = httpException.StatusCode;
                errorResponse.Message = httpException.Message;
            }

            context.Response.StatusCode = (int)errorResponse.StatusCode;
            await context.Response.WriteAsJsonAsync(errorResponse);
        }
    }
}
