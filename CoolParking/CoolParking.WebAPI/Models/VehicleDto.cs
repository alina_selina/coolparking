﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Models
{
    public class VehicleDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        [JsonNumberHandling(JsonNumberHandling.Strict)]
        public decimal Balance { get; set; }

    }
}
