﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Models
{
    public class TopUpVehicleDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("sum")]
        [JsonNumberHandling(JsonNumberHandling.Strict)]
        public decimal Sum { get; set; }
    }
}
