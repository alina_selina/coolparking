﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Models
{
    public class TransactionDto
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [JsonProperty("sum")]
        [JsonNumberHandling(JsonNumberHandling.Strict)]
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
}
